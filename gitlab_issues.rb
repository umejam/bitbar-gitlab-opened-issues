#!/usr/bin/env ruby
# coding: utf-8

# <bitbar.title>GitLab Opened Issues</bitbar.title>
# <bitbar.version>v0.0.1</bitbar.version>
# <bitbar.author>umejam</bitbar.author>
# <bitbar.desc>Show GitLab opened issues</bitbar.desc>
# <bitbar.dependencies>ruby</bitbar.dependencies>
# <bitbar.abouturl>https://gitlab.com/umejam</bitbar.abouturl>

require 'net/http'
require 'uri'
require 'json'

token = ENV['GITLAB_PRIVATE_TOKEN'] || 'YOUR_PRIVATE_TOKEN'
gitlab_url = ENV['GITLAB_URL'] || 'https://gitlab.com'
issue_api_path = 'api/v3/issues?state=opened'
uri = URI.parse("#{gitlab_url}/#{issue_api_path}")

begin
  Net::HTTP.start(uri.host, uri.port) do |http|
    http.use_ssl = true if uri.scheme == 'https'
    req = Net::HTTP::Get.new uri
    req['PRIVATE-TOKEN'] = token
    res = http.request req
    raise "error #{res.code} #{res.message}" if res.code != '200'
    result = JSON.parse(res.body, symbolize_names: true)
    puts "✣ #{result.length} | color=#{result.empty? ? '#a0a0a0' : '#F08080'}"
    puts '---'
    result.each do |issue|
      puts "#{issue[:id]} #{issue[:title]}"
    end
    puts '---'
  end
rescue
  puts '✣ ! | color=#ECB935'
  puts '---'
  puts "Exception: #{$!}"
end
